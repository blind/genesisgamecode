# Yes, it's a git repo #

This repo contains test code for SEGA Mega Drive. Might become a playable game one day.

### Required tools ###

* Python
* PIL (Python image library)
* vasm used for building.

### Tools that will sure be helpful ###

* Tile map files are created with Tiled
* mess used for testing and debugging.

### How to build ###

Install the required tools. Both z80 and m68k of vasm is used, both with motorola syntax.
run make in project folder.

Start the generated .smd file on your device or in appropriate emulator.
