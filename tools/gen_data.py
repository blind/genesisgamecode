#!/usr/bin/env python
#coding: utf-8

import xml.etree.ElementTree as etree
import struct
import tilemapper


class BitStream():
	
	def __init__(self):
		self._array = []
		self._bit_count = 0

	def append_bit( self, bit ):
		if len(self._array) <= self._bit_count:
			self._array.append(0)

		idx = self._bit_count/8

		if bit:
			bit_mask = 1 << (7 - (self._bit_count%8))
			self._array[idx] = self._array[idx] | bit_mask
		self._bit_count += 1

	def get_data(self):
		return self._array

def generate_level_collision_data( tilemap, layername, filename ):
	layer_data = tilemap.layers[layername]

	bit_array = []

	for y in xrange( tilemap.height ):
		y_offset = y*tilemap.width
		row_data = BitStream()
		for x in xrange( tilemap.width ):
			row_data.append_bit( layer_data[y_offset+x] != 0 )

		bit_array.append( row_data.get_data() )

	outfile = open(filename,"w")
	outfile.write( struct.pack( ">HH", len(bit_array[0]), len(bit_array) ))

	for r in bit_array:
		outfile.write( struct.pack( ">%dB"%len(r), *r ) )


if __name__ == "__main__":
	tileset,palette = tilemapper.load_tileset_and_palette( "content/tilesettest.gif" )

	level = tilemapper.TileMap()
	level.load_tmx( "content/level1.tmx" )

	background = tilemapper.TileMap()
	background.load_tmx( "content/background.tmx" )

	used_tiles = level.get_used_tiles()
	bg_used_tiles = background.get_used_tiles()
	used_tiles.update( bg_used_tiles )

	print( "Number of unsed tiles: %d"%len( used_tiles ))

	minimized_tileset = tileset.get_subset( used_tiles )


	# Test some sprite extraction stuff.
	sprite_tiles = tileset.get_subset( [36,37,38,39,40,41,42] )

	minimized_tileset.append( sprite_tiles )


	level.merge_layers( "back", "level", "merged" )

	level.remap( minimized_tileset )
	background.remap( minimized_tileset )

	tilemapper.save_tilemap_md( level, "merged", "build/tm_level1.map", "front", tileset=minimized_tileset )
	tilemapper.save_tilemap_md( background, "level", "build/tm_bg1.map" )

	generate_level_collision_data( level, "level", "build/tc_level1.col" )

	tilemapper.save_tileset_md( minimized_tileset, "build/ts_level1.bin" )
	tilemapper.save_palette_md( palette, "build/ts_palette.bin", count=2 )

