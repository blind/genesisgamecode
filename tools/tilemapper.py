#!/usr/bin/env python

try:
	import Image
except Exception, e:
	from PIL import Image

import sys
import os
import struct
import xml.etree.ElementTree as etree

class Tile():
	def __init__(self):
		self.data = None

	def palette_offset(self):
		min_color_idx = min(self.data)
		max_color_idx = max(self.data)
		offs = min_color_idx/16

		if offs+(max_color_idx - min_color_idx) > 15:
			print( "Warning: block uses colors from more than one palette")
		return offs

	def __eq__(self,other):
		return self.data == other.data

	def addpixel(self,pixel):
		if self.data == None:
			self.data = []
		self.data.append(pixel)

	def pixelcount(self):
		return len( self.data ) or None


class Tileset():
	#tiles = []
	#name = "undefined"
	#_mapper = None

	def __init__(self):
		self.tiles = []
		self.name = "undefined"
		self._mapper = None

	def __iter__(self):
		return iter( self.tiles )
	"""
	class RemapIterator():
		def __init__(self, tileset):
			self.tiles = tileset.tiles
			self.idx = -1
			self.map = iter(tileset._mapper)
		def __iter__(self):
			return self
		def next(self):
			while True:
				nxt = self.map.next()
				self.idx += 1
				if nxt == self.idx:
					break
			return self.tiles[ nxt ]

	def remapped(self):
		return self.RemapIterator( self )
	"""
	def addtile(self,*tile):
		for t in tile:
			self.tiles.append( t )

	def append( self, tileset ):
		for tile in tileset:
			self.tiles.append(tile)

	def get_tile(self, idx ):
		return self.tiles[ self.get_mapped_index( idx )]

	def get_mapped_index( self, idx ):
		if self._mapper != None:
			return self._mapper[idx] if idx in self._mapper else idx
		else:
			return idx
	"""
	def remap_duplicates(self):
		tile_count = len(self.tiles)
		_mapper = range( tile_count )
		duplicates_found = 0
		for x in xrange( len(self.tiles)):
			if _mapper[x] == x:
				current = self.tiles[x]

				# not remapped already.
				for dup_idx in xrange( x+1, tile_count ):
					other = self.tiles[dup_idx]
					if other == current:
						duplicates_found+=1
						_mapper[dup_idx] = x
		self._mapper = _mapper
		print( "Found %d duplicate tiles"%duplicates_found )
	"""
	def get_subset(self, tileindices):
		newtileset = Tileset()
		sorted_indices = list(tileindices)
		sorted_indices.sort()
		subset = [ self.tiles[t] for t in sorted_indices ]
		newtileset.addtile( *subset )
		# This will not do what I want, I have to update the _mapper of newtileset 
		# to point old indices to new ones.

		remap = {}
		for i,tile in enumerate(sorted_indices):
			remap[tile] = i

		newtileset._mapper = remap

		return newtileset


class TileMap():
	def __init__(self):
		self.width = 0
		self.height = 0
		self.layers = {}


	def load_tmx(self,filename):
		infile = open(filename,"r")
		xmldata = infile.read()
		infile.close()

		xml = etree.fromstring(xmldata)
		# width and height of map.
		self.width = int(xml.attrib["width"])
		self.height = int(xml.attrib["height"])

		layers = xml.findall( ".//layer")

		for layernode in layers:
			layer_name = layernode.attrib["name"]
			level_text = layernode[0].text

			# Split map data into an array.
			tiles = str.split(level_text,',')
			tiles = [int(a) for a in tiles]

			# Since tiled uses 0 as "empty" and 1 as first tile,
			# I decrease all indices > 0
			def c(x):
				if x > 0:
					return x-1
				else:
					return x
			tiles = map( c, tiles )

			self.layers[layer_name] = tiles
			print( "Loaded layer %s from %s"%(layer_name, filename) )


	def merge_layers( self, back_layer, front_layer, result_layer, merge_function = None ):
		bg = self.layers[back_layer]
		fg = self.layers[front_layer]

		def default_merge(bg,fg): return bg if fg==0 else fg

		mergef = merge_function if merge_function != None else default_merge

		new_layer = []

		for y in xrange( self.height ):
			for x in xrange( self.width ):
				idx = y*self.width+x
				new_layer.append( mergef( bg[idx], fg[idx] ) )

		self.layers[result_layer] = new_layer

	def get_used_tiles(self, *layers ):
		if len(layers) == 0:
			maps = [ v for k,v in self.layers.iteritems() ]
		else:
			maps = [ self.layers[n] for n in layers ]

		used = set()
		used.update( *maps )
		return used

	def delete_layer( self, layer ):
		if layer in self.layers:
			del self.layers[layer]

	def remap( self, tileset ):
		for lname,mapdata in self.layers.iteritems():
			for i,val in enumerate(mapdata):
				mapdata[i] = tileset.get_mapped_index( val )


def save_tilemap_md( tilemap, layer, filename, fg_layer=None, tileset=None ):

	outfile = open( filename,"wb")


	# Ok, some special stuffs, we want to merge fg_layer, but set priority flag
	# in export.
	if fg_layer != None:
		def fg_merge(bg,fg):
			if( fg == 0 ):
				return bg
			else:
				return fg + 0x8000 # This could be the prio flag.

		tilemap.merge_layers( layer, fg_layer, "_tmp_layer", fg_merge )
		layer = "_tmp_layer"

	tiles = tilemap.layers[layer]
	count = len(tiles)

	tilemap.delete_layer("_tmp_layer")

	# If a tileset was passed, we can use it to extract palette for each tile.
	if tileset != None:
		paletted_tiles = []

		for idx in xrange(0,count):
			tile_num = tiles[idx]
			tile = tileset.get_tile(tile_num&0x07ff)
			palette = tile.palette_offset()
			paletted_tiles.append( tile_num | (palette &3)<<13 )
		tiles = paletted_tiles


	# write witdh and height of map.
	outfile.write( struct.pack( ">2H", tilemap.width , tilemap.height ))

	outfile.write( struct.pack(">%sH"%(count), *tiles ) )
	outfile.close()


def save_tileset_md( tileset, filename ):

	# Until the map code is merged with the and uses the updated
	# tiles indices after the remap, we must save "all" tiles
	outfile = open( filename, "wb")

	"""
	if remap:
		# Tileset will be changed to an iterator
		tileset = tileset.remapped()
	"""
	for tile in tileset:
		pixel_count = tile.pixelcount()
		packed_pixels = []
		data = tile.data
		for i in xrange( 0, pixel_count,2 ):
			packed_pixels.append( (data[i] & 0xf)<<4 | data[i+1] &0xf )

		packed_data = struct.pack( ">%dB"%(len(packed_pixels)), *packed_pixels )
		outfile.write( packed_data )

	outfile.close()

def save_palette_md( palette, filename, count=1, offset=0):

	pal_out = open( filename, "wb" )
	mega_palette = []
	for idx in xrange(offset,offset+count*16):
		(r,g,b) = palette.getpixel((idx,0))
		# Mega drive palette has 3 bits per channel in 0000 bbb0 ggg0 rrr0 format 
		mega_color = ((b>>4)<<8 | (g>>4)<<4 | (r>>4)) & 0x0eee
		mega_palette.append( mega_color )

	pal_out.write( struct.pack(">%dH"%(count*16), *mega_palette ) )
	pal_out.close()


def load_tileset_and_palette(filename):
	img = Image.open( filename)
	width,height = img.size

	# Select gif frame
	img.seek(0)

	tileset = Tileset()

	for tile_y in xrange( 0, height/8 ):
		for tile_x in xrange( 0,width/8 ):
			tile_offset_x = tile_x*8
			tile_offset_y = tile_y*8

			tile = Tile()

			for tile_pos_y in xrange( tile_offset_y, tile_offset_y+8 ):
				for tile_pos_x in xrange( tile_offset_x, tile_offset_x+8 ):
					pix = img.getpixel( (tile_pos_x,tile_pos_y) )
					tile.addpixel( pix )

			tileset.addtile( tile )

	# Found a really ugly way to get the colors in the palette in the official documentation.
	lut = img.resize((256, 1))
	lut.putdata(range(256))
	lut = lut.convert("RGB").getdata()

	return tileset,lut


def main():
	if len(sys.argv) < 3:
		print( "usage: <inputfile> <outputfile>")
		sys.exit(-1)

	my_tiles,palette = load_tileset_and_palette( sys.argv[1] )

	# Just dump to disk for now, in mega drive format, each pixel is 4 bits.

	my_tiles.remap_duplicates()

	#used_tiles = set(my_tiles._mapper)
	#print( "Number of unique tiles: %d"%len( used_tiles ))


	# TEST
	"""
	print( "remapped" )
	for t in my_tiles.remapped():
		print t
	print("original")
	for t in my_tiles:
		print t
	"""

	save_tileset_md( my_tiles, sys.argv[2] )

	# Dump palette to tmp file for now.

	save_palette_md( palette, "ts_palette.bin")

if __name__ == '__main__':
	main()

