

move_vram_addr	MACRO
	move.l	#((((\1)&$3fff)<<16)+(((\1)>>14)&3))|(1<<30),\2
	ENDM


move_cram_addr	MACRO
	move.l	#((((\1)&$3fff)<<16)+(((\1)>>14)&3))|(3<<30),\2
	ENDM


Z80_stopwait	MACRO
;.\@	move.w	#$100,$a11100		; 
;	btst	#1,$a11101
;	bne.s	.\@
	ENDM

Z80_start		MACRO
;	move.w	#$0,$a11100
	ENDM


	; Try to set background color or something...

VDP_BASE	equ	$c00000
VDP_DATA	equ	VDP_BASE
VDP_CTRL	equ	VDP_BASE+4


PLANE_A_ADDR	equ	$c000
PLANE_B_ADDR	equ	$e000
PLANE_W_ADDR	equ	$d000
SPR_ATTR_ADDR	equ	$b800
HSCROLL_ADDR	equ	$1400
