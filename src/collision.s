


	include "sprt.i"


	section text


;---------------------------------------------------------
; Calculate list of tile indices that is occupided by sprite bounding box.
; params:  a0   - animation frame pointer.
;          a1   - out pointer to tile index pairs x,y (word sized)
;          d0.w - X world position 
;          d1.w - Y world position 
; returns: d0.w - number of tiles written to a1

get_intersecting_tiles:

	movem.l	d1-7/a0-1,-(sp)

	sub.w	FRME_hotspot_x(a0),d0	; top left corner 
	sub.w	FRME_hotspot_y(a0),d1	; 

	moveq	#0,d5				; d5, count for number of pairs written.
	moveq	#3,d4				; d4 as constant #3
;	xb = x/8
;	yb = y/8

	move.w	d0,d2
	lsr.w	#3,d2				; d2 = pixel x/8 -> tile x

	move.w	d1,d3
	lsr.w	#3,d3				; d3 = pixel y/8 -> tile y

;	cx = (sprite["w"]/8) + (1 if x%8 != 0 else 0)
;	cy = (sprite["h"]/8) + (1 if y%8 != 0 else 0)

	move.l	d5,d6		 		; clear d6 reg

	move.b	FRME_width(a0),d6
	lsr.w	#3,d6
	and.w	#3,d0
	beq.s	.no_add_x
	addq.w	#1,d6
.no_add_x:
	move.w	d6,d4

	move.l	d5,d7		 		; clear d7 reg

	move.b	FRME_height(a0),d7
	lsr.w	#3,d7
	and.w	#3,d1
	beq.s	.no_add_y
	addq.w	#1,d7
.no_add_y:


;	for dy in xrange(cy):
	subq.w	#1,d7
.y_loop:
;		xb_temp = xb
	move.w	d2,d1		; d1 = xb_temp
;		for dx in xrange(cx):
	move.w	d4,d6
	subq.w	#1,d6
.x_loop
;			result.append((xb_temp,yb))
	move.w	d1,(a1)+
	move.w	d3,(a1)+
	addq.w	#1,d5
;			xb_temp += 1
	addq.w	#1,d1

	dbra.w	d6,.x_loop
;		yb += 1
	addq.w	#1,d3

	dbra.w	d7,.y_loop

	move.w	d5,d0
	movem.l	(sp)+,d1-7/a0-1

	rts

