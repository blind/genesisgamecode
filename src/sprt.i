
;---------------------------------------------------------
; Structs for sprites:
	rsreset
SPRT_y		rs.w	1		; Low 10 bits used. 128 = first visible col
SPRT_size	rs.b	1		; 0-1 vertical size, 2-3 horizontal size
SPRT_link	rs.b	1		; 0-6 link, 8-9 vertical size, 10-11 horizontal size
SPRT_pandp	rs.w	1		; 0-10 pattern, 11 h-flip, 12 v-flip, 13-14 palt, 15 prio
SPRT_x		rs.w	1		; Low 9 bits used. 128 0 first visible line.


; Animation frame 
	rsreset
FRME_hotspot_x		rs.w	1
FRME_hotspot_y		rs.w	1

FRME_width			rs.b	1
FRME_height			rs.b	1
FRME_vdp_size		rs.b	1	; bit 0-1 bits are vertical size, bit 2-3 are horizontal 
FRME_vdp_palette	rs.b	1	; bit [0..1] palette (prio and flip bits should be set from logic)
FRME_vdp_pattern	rs.w	1	; bits [0..10] pattern position (from offset?)

