;---------------------------------------------------------
;
;
;
;
;---------------------------------------------------------

	include 'macros.i'
	include 'sprt.i'

	xdef main

	section text

;---------------------------------------------------------
;---------------------------------------------------------
main:
	trap 	#0

	move.w	#130,player1
	move.w	#119,player1+M_y

	bsr		dma_init

	bsr		smem_init

	move.w	#SPR_ATTR_ADDR,d0
	bsr		sprt_init

	bsr		setup_vdp

	bsr		controller_init

	lea		palette_1,a0
	moveq	#0,d0
	bsr		load_palette

	lea		palette_1+32,a0
	moveq	#1,d0
	bsr		load_palette

	move.w	#1,d0
	lea		sprite_id,a0
	bsr		sprt_alloc

	; upload the tile data to VRAM.
	; not using DMA for simplicity.


	lea		VDP_BASE,a0
	lea		tile_set,a1
	move.w	#$8f02,4(a0)
	move_vram_addr	0,4(a0)	; Load tileset at VRAM addr 0
.moar:
	move.w	(a1)+,(a0)
	cmpa.l	#tile_set_end,a1
	blt.s	.moar

	bsr		prepare_level

	bsr 	prepare_bg_b


	move.w	#PLAYER_ST_IN_AIR,player_state


	move.l	#my_vbl_rout,d0
	bsr		install_vbi_handler

	move.w	#$2300,d0
	trap	#1				; Set SR register with content of d0


; Initialize some 

;---------------------------------------------------------

main_loop:
	;
	bsr		wait_vbl
;	move.w	#1,vbl_flag
;	trap	#2
	; --

	bsr		smem_reset

	bsr		read_controller

	bsr.s	setup_rasters

	bsr		update_player
	bsr		update_background
	bsr		update_level_display

	bsr		send_player_to_renderer

	bsr		sprt_render
	bra.s	main_loop


;---------------------------------------------------------
setup_rasters:

	; Setup HBI.
	move.l	#hbl1,d0
	bsr		install_hbi_handler

	movea.l	#VDP_CTRL,a0
	move.w	#$8f00,(a0)		; 0 byte update each write.
	move.w	#$8a08,(a0)		; HBI after 128 lines
	move.w	#$8014,(a0)		; enable HBI.
	move_cram_addr 0,(a0)

	lea		raster_colors,a1
	move.w	(a1)+,d0
	move.w	d0,$c00000

	move.w	(a1)+,hbl_color_set
	move.l	a1,raster_color_ptr

	rts

;---------------------------------------------------------
;---------------------------------------------------------
wait_vbl:
	move.w	#1,vbl_flag
.wait
	tst.w	vbl_flag
	bne.s	.wait
	rts
;---------------------------------------------------------
;---------------------------------------------------------

hbl1:
	move.w	#$8a10,$c00004		; HBI at next lines - only latch _after_ the next HBL,
	move_cram_addr 0,$c00004	; It should be forbidden for anyone to write to these registers 
	move.l	d0,-(sp)
	move.l	#hbl2,d0
	bsr		install_hbi_handler
	move.l	(sp)+,d0
	rte

hbl2:
	move.w	hbl_color_set,$c00000
	movem.l	d0/a0,-(sp)
	move.l	raster_color_ptr,a0
	move.w	(a0)+,hbl_color_set
	move.l	a0,raster_color_ptr	
	movem.l	(sp)+,d0/a0

	rte

;---------------------------------------------------------
;---------------------------------------------------------

; Animation data
	rsreset
frame_count		rs.w	1
frames_list		rs.l	1


PLAYER_ST_GROUNDED	EQU	0
PLAYER_ST_IN_AIR	EQU	1


; Temporary placement for an animation frame...


	section data

frame1:
	dc.w	7		; hotspot x
	dc.w	15		; hotspot y
	dc.b	16		; 16 pixels width
	dc.b	16		; 16 pixels high
	dc.b	5		; 2*2 tiles
	dc.b	0		; palette, should perhaps not be frame specific
	dc.w	12		; pattern

	even

	section text


	rsreset
M_x			rs.l	1
M_y			rs.l	1
M_jmpstat	rs.w	1
M_wlkstat	rs.w	1
M_size		rs.b	1

;---------------------------------------------------------
update_player:

	; Allocate a buffer for the calculated end position,
	; to be fed into the collision handler
	moveq	#16,d0
	bsr		smem_alloc
	move.l	d0,a4
	move.l	a4,a0

	lea		player1,a1

	move.l	(a1)+,(a4)+		; x
	move.l	(a1)+,(a4)+		; y
	move.l	(a1)+,(a4)+		; jmpstat 0 wlkstat

	move.w	player_state,d0
	lsl.w	#2,d0
	lea		handler_lut,a1
	move.l	(a1,d0),a1
	jsr		(a1)


	; Do collision handling.

	cmp.w	#PLAYER_ST_IN_AIR,player_state
	bne.w	.no_fall

	; Hardcoded floor plane.
	move.w	M_y(a0),d0
	cmp.w	#191,d0
	blt.s	.not_landed
	move.l	#191<<16,d0
	move.l	d0,M_y(a0)
	move.w	#0,M_jmpstat(a0)
	move.w	#PLAYER_ST_GROUNDED,player_state
.not_landed
.no_fall

	; The newly calculated positions should be in (a0)

	lea		player1,a1

	move.l	(a0)+,(a1)+		; x
	move.l	(a0)+,(a1)+		; y
	move.l	(a0)+,(a1)+		; jmpstat 0 wlkstat


	rts



;---------------------------------------------------------

handler_lut:
	dc.l	handle_st_grounded
	dc.l	handle_st_in_air

;---------------------------------------------------------
;---------------------------------------------------------
; input states.
handle_st_grounded:
	bsr.s	calculate_horizontal_movement

	move.w	current_controller_changed,d0
	andi.w	#INP_B1_BIT,d0
	beq.s	.no_jump

	bsr		init_jump
	bsr 	calculate_vertical_movement
.no_jump

	rts


calculate_horizontal_movement:

	lea		_walk_speed_lut,a1
	move.w	current_controller_status,d0
	andi.w	#INP_RIGHT1_BIT,d0
	bne.s .no_walk_right

	move.w	M_wlkstat(a0),d0
	add.w	#1,d0
	cmp.w	#3,d0
	ble.s	.in_bound
	move.w	#3,d0
.in_bound
	move.w	d0,M_wlkstat(a0)
	lsl.w	#2,d0
	lea		(a1,d0),a2
	move.l	(a2),d0
	bra.s	.done_walk
.no_walk_right:

	move.w	current_controller_status,d0
	andi.w	#INP_LEFT1_BIT,d0
	bne.s	.no_walk

	move.w	M_wlkstat(a0),d0
	sub.w	#1,d0
	cmp.w	#-3,d0
	bge.s	.in_bound_low
	move.w	#-3,d0
.in_bound_low
	move.w	d0,M_wlkstat(a0)
	asl.w	#2,d0
	lea		(a1,d0),a2
	move.l	(a2),d0
	bra.s	.done_walk

.no_walk
	move.w	M_wlkstat(a0),d0
	beq.s	.still
	bmi.s	.done_left

	sub.w	#1,d0
	move.w	d0,M_wlkstat(a0)
	lsl.w	#2,d0
	move.l	(a1,d0.w),d0
	bra.s	.done_walk
.done_left
	add.w	#1,d0
	move.w	d0,M_wlkstat(a0)
	lsl.w	#2,d0
	move.l	(a1,d0.w),d0

.done_walk
	move.l	(a0),d1		; Get X-position
	add.l	d0,d1
	move.l	d1,(a0)		; a0 points to buffer where 
.still
	rts

	section data

	dc.l	-$00010000
	dc.l	-$00008000
	dc.l	-$00002000
_walk_speed_lut:
	dc.l	$00000000
	dc.l	$00002000
	dc.l	$00008000
	dc.l	$00010000


;---------------------------------------------------------

	section text

	REPT 10
	nop
	ENDR


handle_st_in_air:

	bsr		calculate_horizontal_movement
	bsr 	calculate_vertical_movement
	rts


init_jump:
	move.w	#1,M_jmpstat(a0)
	rts

calculate_vertical_movement:

	move.w	M_jmpstat(a0),d0		; flags and state.
	andi.w	#1,d0
	beq.s	.was_falling			; flag 0 -> falling

	; Check if the player released the jump
	; button, if so end jump.
	move.w	current_controller_status,d0
	andi.w	#INP_B1_BIT,d0
	bne.s	.end_jump

	moveq	#0,d1
	move.b	M_jmpstat(a0),d1
	move.w	d1,d0
	cmp.w	jump_move_lut_len,d0	; did we reach peak?
	ble		.end_jump	
	addq.b	#1,d0					; increase counter
	move.b	d0,M_jmpstat(a0)		; write back

	lea		jump_move_lut,a1
	; TODO: maybe use d1*4 as index in a1, and make a real offset table.
	move.l	(a1),d0					; change in y
	add.l	M_y(a0),d0
	move.l	d0,M_y(a0)

	bra.s	.done					; we have moved

.end_jump
	move.w	#1,d0
	move.w	d0,M_jmpstat(a0)

	; falling state.
	; here we could check if double jump is valid 

.was_falling

	; Just for now.
	move.l	M_y(a0),d0
	add.l	jump_peak,d0
	move.l	d0,M_y(a0)

.done
	rts

	section data

end_jump_lut_ptr:
	dc.l	jump_peak

jump_move_lut_len:
	dc.w	30

jump_move_lut:
	dc.l	-$00020000

jump_peak:
	dc.l	$00020000

;---------------------------------------------------------
send_player_to_renderer:
	; The sprite position is relative to world map
	; therefore we must subtract the map pos to get the correct pos.

	move.w	player1,d1			; x pos
	move.w	player1+M_y,d2		; y pos

	sub.w	current_display_pos,d1
	sub.w	current_display_pos+2,d2

	lea		frame1,a1

	sub.w	FRME_hotspot_x(a1),d1
	sub.w	FRME_hotspot_y(a1),d2

	add.w	#$80,d1
	add.w	#$80,d2

	move.w	sprite_id,d0
	bsr		sprt_get_attribute_ptr
	; a0 is now pointer to sprite attribute.	
	move.w	d1,SPRT_x(a0)
	move.w	d2,SPRT_y(a0)

	move.b	FRME_vdp_size(a1),d1
	move.b	d1,SPRT_size(a0)

	move.w	FRME_vdp_pattern(a1),d1
	move.w	d1,SPRT_pandp(a0)

	bsr		sprt_add_to_render

	rts

;--------------------------
update_background:
	lea		plane_b_scroll_speeds,a0
	lea		plane_b_scroll,a1

	move.w	#48*2,d0
	bsr		smem_alloc
	move.l	d0,a2
	move.l	d0,a3

	moveq	#28-1,d1
.np	
	move.l	(a1),d0
	add.l	(a0)+,d0
	move.l	d0,(a1)+
	swap	d0
	move.w	d0,(a2)+
	dbra.w	d1,.np

	move.w	#28,d0
	move.w	#4*8,d1
	move.l	a3,a0
	movea.w	#$1402,a1

	bsr		dma_ram2vram_req

	rts

;--------------------------
; Init tile map
; upload tiles from map to VRAM
prepare_bg_b:
	lea		tile_map_bg,a0
	move.w	#$8f02,VDP_CTRL		; Skip 64 bytes per write
	move_vram_addr	PLANE_B_ADDR,d4		; d4 - select VRAM addr
	move.w	(a0)+,d0		; width in tiles in d0
	move.w	(a0)+,d1		; height of tiles in d1

	moveq	#28-1,d6
.column_loop:
	move.l	d4,VDP_CTRL
	moveq	#64-1,d7
.row_loop:
	move.w	(a0)+,d0
	move.w	d0,VDP_DATA
	dbra.w	d7,.row_loop
	add.l	#(128<<16),d4
	dbra.w	d6,.column_loop

	rts


;----------------------------------------------------
;----------------------------------------------------
; Init tile map
; upload tiles from map to VRAM
prepare_level:
	lea		tile_map_level,a0
	move.w	#$8f02,VDP_CTRL		; Skip 64 bytes per write
	move_vram_addr	PLANE_A_ADDR,d4	; d4 - select VRAM addr


	move.w	(a0)+,d0		; width in tiles in d0
	move.w	(a0)+,d1		; height of tiles in d1

	sub.w	#40,d0			; skip 40 tiles, since we already drew them.
	add.w	d0,d0			; width in words.

	moveq	#28-1,d6
.column_loop:
	move.l	d4,VDP_CTRL
	moveq	#40-1,d7
.row_loop:
	move.w	(a0)+,VDP_DATA
	dbra.w	d7,.row_loop
	add.l	#(128<<16),d4
	adda.w	d0,a0
	dbra.w	d6,.column_loop


	; Setup map coordinates
	move.l	#0,current_display_pos			; clear both x and y offsets
	move.l	#tile_map_level,current_map

	rts

;----------------------------------------------------
; 
update_level_display:

	lea		last_display_pos,a6
	lea		current_display_pos,a5

	; See if sprite position should force scrolling of map.
	lea		player1,a0	;
	move.w	(a0),d0
	sub.w	(a6),d0		; 
	moveq	#120,d1		; window left 50pix
	sub.w	d0,d1
	bmi.s	.no_left_move

	; Scroll left to follow player sprite.

	sub.w	d1,(a5)		; Move 
	bpl.s	.no_right_move	; Don't move left of screen.
	move.w	#0,(a5)

	bra.s	.no_right_move
.no_left_move

	move.w	(a6),d0		; Screen x pos left corner
	add.w	#256,d0		; Add screen width for right corner.
	sub.w	(a0),d0		; (right corner - player pos)
	moveq	#120,d1
	sub.w	d0,d1		; 50 - (right corner - player pos)
	bmi.s	.no_right_move

	move	tile_map_level,d2	; tile map width.
	lsl.w	#3,d2			; width * 8 for pixel width
	sub.w	#256,d2

	move.w	d1,d0
	add.w	(a5),d1
	sub.w	d1,d2
	bmi.s	.no_right_move

	add.w	d0,(a5)

.no_right_move


	; Start by calculating the changes in horizontal position.

	move.w	(a5),d0		; new pos
	move.w	(a6),d1		; old pos
	move.w	d0,d2
	sub.w	d1,d2		; new pos - old pos -> change in d2
	beq		.no_horizontal_change
	bmi		.scroll_right

	; scrolling to the left.
	move.w	#80,d6
	bra.s	.calculate_scroll
.scroll_right
	move.w	#-2,d6
.calculate_scroll



	; If old_pos%8 was 0 or delta > 8, we need to copy data from
	; level map to screen. Since map data is stored horizontally,
	; lets first copy the columns to a buffer in ram so they
	; can be DMAed from there at VBL.

	; NOTE: For now only support max 8 pixel scroll per frame!!!
	; TODO: Don't do what the note above says.

	move.b	d1,d3
	and.w	#$3,d3		; d3 -> old_pos%8
	beq		.must_copy_col

	; if (new_pos ^ old_pos)>>3 == 0 then skip copy.

	move.w	d0,d2
	eor.w	d1,d2
	and.w	#$fffc,d2

	beq		.no_copy_col

.must_copy_col

	move.l	current_map,a2		; a2 points to level data.
	; column to copy is at offset (new_pos/8)*2 + (new_pos_y*map_width)
	; lets ignore the y pos for now.
	move.w	d0,d3		; new_pos in d3
	lsr.w	#3,d3		; /8
	lsl.w	#1,d3		; *2

	lea		4(a2),a3
	adda.w	d6,a3		; magic!
	adda.w	d3,a3		; a3 should point to the first block to copy.
	move.w	(a2),d4		; d4 = number of blocks per row in map
	add.w	d4,d4		; 2 bytes per block

	move.l	d0,-(sp)

	move.w	#29*2,d0
	bsr		smem_alloc

	move.l	d0,a0		; allocated buffer in a0 and d0
	move.w	#28-1,d1	; loop 28 times
.fill_dma_buffer_loop2
	move.w	(a3),(a0)+
	adda.w	d4,a3
	dbra.w	d1,.fill_dma_buffer_loop2

	; register DMA events
	move.l	d0,a0		; source pointer in a0
	move.w	#28,d0		; 28 words.
	move.w	#64*2,d1	; increase by 128

	move.w	#PLANE_A_ADDR,a1	; VRAM layer A base address, should not be hard coded!
	; offset in VRAM is (new_pos/8)%64*2
	add.w	d6,d3
	andi.w	#(64-1)<<1,d3
	adda.w	d3,a1

	bsr		dma_ram2vram_req


	move.l	(sp)+,d0

.no_copy_col
	; Fill plane a hscroll dma buffer

	; Currently the code uses 64*32 cell display, so that is what the
	; width we are using.

	; Allocated memory for DMA source buffer.
	move.w	#28*2,d0
	bsr		smem_alloc
	move.l	d0,a2
	move.l	a2,a3

	; a5 must still point to new screen position.
	move.w	(a5),d2
	neg.w	d2			; panning to right = -x movment.
	and.w	#(64*8)-1,d2	; mask unused bits
	moveq	#28-1,d1
.fill_dma_buffer_loop
	move.w	d2,(a2)+
	dbra.w	d1,.fill_dma_buffer_loop

	; Setup dma request
	move.w	#28,d0		; Number of bytes to copy.
	move.w	#32,d1		; bytes to skip after each word write.
	move.l	a3,a0		; source 
	movea.w	#$1400,a1	; Please don't hard code this...
	bsr		dma_ram2vram_req

.no_horizontal_change

	move.l	(a5),(a6)	; Set current pos as last for next frame.


	rts


;----------------------------------------------------
;----------------------------------------------------
; Read controller
; Code copied from interwebs, haven't read up on 
; _how_ it works, it just does.
read_controller:
	
	Z80_stopwait
	movea.l	#$a10003,a0
	move.b	#0,(a0)			; TH to low...
	move.w	#$0030,d1		; mask all but Start and A
	nop
	nop
	and.b	(a0),d1			; 00SA00DU 
	lsl.w	#2,d1			; make room for BCRL

	move.b	#$40,(a0)		; TH to high -> x1CBRLDU
	move.w	#$003f,d0		; mask	
	nop
	nop
	and.b	(a0),d0			; read and mask
	or.w	d1,d0

	Z80_start
	move.w	current_controller_status,d1
	move.w	d0,current_controller_status
	eor.w	d0,d1
	move.w	d1,current_controller_changed

	rts


controller_init:
	Z80_stopwait

	moveq	#$40,d0		; bit mask for controller init.
	; Init code from sgdk, converted to assembler by me.
	
	lea		$a10009,a0
	move.b	d0,(a0)
	move.b	d0,2(a0)
	move.b	d0,4(a0)

	lea		$a10003,a0
	move.b	d0,(a0)
	move.b	d0,2(a0)
	move.b	d0,4(a0)
	Z80_start
    rts

;---------------------------

setup_vdp:
	lea		VDP_CTRL,a1
	lea		vdp_regs,a0
	move.w	#((vdp_regs_end-vdp_regs)/2)-1,d2
.copy_loop:
	move.w	(a0)+,(a1)
	dbra.w	d2,.copy_loop
	rts

;---------------------------

; Load 16 colour palette
; params:
; d0 - palette index
; a0 - palette data

load_palette:
	lea		VDP_BASE,a1
	move.w	#$8f02,4(a1)	; set auto-increment to 2.
	and.l	#$3,d0
	lsl.w	#5,d0
	swap.w	d0
	add.l	#$c0000000,d0
	move.l	d0,4(a1)
	moveq	#15,d1
.copy_loop
	move.w	(a0)+,(a1)
	dbra.w	d1,.copy_loop
	rts


; vbl_routine:
my_vbl_rout:
	tst.w	vbl_flag
	beq.s	.no_wait
	movem.l	d0/a0-1,-(sp)
	; directly after vbl, we need to start DMA
	bsr		dma_execute_queue
	; then we need to update the VDP registers.
	movem.l	(sp)+,d0/a0-1

	clr.w	vbl_flag
.no_wait
	rte


;---------------------------------------------------------
;---------------------------------------------------------
; Short time memory allocations, only valid for one frame.
;---------------------------------------------------------
;
; Parameters:
; d0.w - amount of bytes to allocate.
; Returns:
; d0.l - pointer to allocated memory
smem_alloc:
	move.l	d1,-(sp)
	moveq.l	#1,d1
	; make sure the size is even.
	add.w	d0,d1
	andi.b	#$fe,d1
	move.l	_smem_free_ptr,d0
	add.l	d0,d1
	move.l	d1,_smem_free_ptr
	move.l	(sp)+,d1
	rts

smem_init:
smem_reset:
	move.l	#_smem_frame_data,_smem_free_ptr
	rts


;---------------------------------------------------------
;---------------------------------------------------------
;---------------------------------------------------------
	section data
;---------------------------------------------------------

palette_1:
	incbin	ts_palette.bin

; http://md.squee.co/wiki/VDP
vdp_regs:
	dc.w	$8004					; mode register 1 - DMA enabled
	dc.w	$8174					; mode register 2 - Display enable bit set ($40) + VBI ($20)
	dc.w	$8200+(PLANE_A_ADDR>>10)	; plane a table location
	dc.w	$830c						; window table location -  VRAM:$3000
	dc.w	$8400+(PLANE_B_ADDR>>13)	; plane b table location
	dc.w	$8500+(SPR_ATTR_ADDR>>9)	; sprite table location
	dc.w	$8600						; sprite pattern generator base addr. 
						; (should be 0, only used on modified machines with more VRAM)
	dc.w	$8700		; backgroud colour,  (reg 7)
	dc.w	$8a00		; HBL IRQ controller
	dc.w	$8b02		; Mode register 3
	dc.w	$8c00		; mode register 4 (320 wide display)
	dc.w	$8d05		; HBL scroll data location (VRAM:$1400)
	dc.w	$8e00		; nametable pattern gen base addr.
	dc.w	$8f02		; auto-increment value
	dc.w	$9001		; plane size
	dc.w	$9100		; window plane h-pos
	dc.w	$9200		; window place v-pos
vdp_regs_end:

z80_code:
	incbin "z80-init.bin"

tile_set:
	include 'tileset.s'
tile_set_end:

tile_map_level:
	incbin	"tm_level1.map"

tile_map_bg:
	incbin	"tm_bg1.map"

raster_colors:
	dc.w	$0100*2		; dark blue
	dc.w	$0200*2		; 
	dc.w	$0300*2		; 
	dc.w	$0400*2		; 
	dc.w	$0500*2		; 
	dc.w	$0610*2		; 
	dc.w	$0621*2		; 
	dc.w	$0622*2		; 
	dc.w	$0623*2		; 
	dc.w	$0624*2		; 
	dc.w	$0625*2		; 
	dc.w	$0625*2		; 
	dc.w	$0625*2		; 
	dc.w	$0625*2		; 



; Some fixed point scroll speed for plane b.
plane_b_scroll_speeds:
	dc.l	-$00000400
	dc.l	-$00000800
	dc.l	-$00001000
	dc.l	-$00002000
	dc.l	-$00002000
	dc.l	-$00002000
	dc.l	-$00002000
	dc.l	-$00002000
	dc.l	-$00002000
	dc.l	-$00002000
	dc.l	-$00002000
	dc.l	-$00002000
	dc.l	-$00002000
	dc.l	-$00002000
	dc.l	-$00002000
	dc.l	-$00002000
	dc.l	-$00002000
	dc.l	-$00002000
	dc.l	-$00002000
	dc.l	-$00002000
	dc.l	-$00002000
	dc.l	-$00002000
	dc.l	-$00002000
	dc.l	-$00008000
	dc.l	-$00010000
	dc.l	-$00018000
	dc.l	-$00020000
	dc.l	-$00028000


INP_UP1_BIT		equ	(1<<0)
INP_DOWN1_BIT	equ (1<<1)
INP_LEFT1_BIT	equ (1<<2)
INP_RIGHT1_BIT	equ (1<<3)

INP_B1_BIT		equ (1<<4)
INP_C1_BIT		equ (1<<5)
INP_A1_BIT		equ (1<<6)
INP_START1_BIT	equ (1<<7)


;---------------------------------------------------------
;---------------------------------------------------------
; RAM  - only ds.? allowed here
;---------------------------------------------------------
	section bss
;---------------------------------------------------------

vbl_flag:			ds.w	1
hscroll_value_b		ds.w	1
vscroll_value		ds.w	1

current_controller_status	ds.w	1
current_controller_changed	ds.w	1

raster_color_ptr	ds.l	1
hbl_color_set		ds.w	1

; Map and scroll position variables.
; We need to keep track of what map coordinates is a the top left
; corner of the screen, and the fine scroll values inside that tile.

current_display_pos:
	ds.w	1			; offset in pixels in x coordinate within map
	ds.w	1			; offset in pixels in y coordinate within map

last_display_pos:
	ds.w	1
	ds.w	1

current_map:
	ds.l	1			; address to current map

; Position of the player sprite. Should be in map coordinates.

player1:
	ds.l	M_size

player_state:
	ds.w	1

sprite_id
	ds.w	1

plane_b_scroll:
	ds.l	28

_smem_free_ptr:
	ds.l	1
_smem_frame_data:
	ds.b	4096		; buffer to allocate memory from that will only be valid for one frame.
	even


