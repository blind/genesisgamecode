
MESS_OPTIONS:= -cfg_directory tools/mess/cfg -nvram_directory tools/mess/nvram

OUT=m3game.smd

VASM68K=vasmm68k_mot

VASM_FLAGS=-quiet -Ibuild -Isrc 



SRCS=sys.s \
	main.s \
	dma.s \
	sprt.s \
	collision.s

OBJS=$(addprefix build/,$(SRCS:.s=.o))

.PHONY: all z80 clean test debug data deps

all: data build $(OUT)

build:
	@mkdir -p build

$(OUT): z80 data $(OBJS)
	vlink -o $(OUT) -T md.lnk -b rawbin1 $(OBJS)
	@python tools/gen_checksum.py $(OUT)

build/%.o: src/%.s
	$(VASM68K) $(VASM_FLAGS) -Fvobj -o $@ $<

data: build/data

build/data: content/level1.tmx content/background.tmx content/tilesettest.gif
	@python tools/gen_data.py
	touch build/data

z80: src/z80-init.asm
	vasmz80_mot -quiet -Fbin -o build/z80-init.bin src/z80-init.asm

clean:
	rm -f $(OUT) build/*

test: $(OUT)
	@mess64 genesis -cart $(OUT) $(MESS_OPTIONS) -waitvsync

teste: $(OUT)
	@mess64 megadriv -cart $(OUT) $(MESS_OPTIONS)

debug: $(OUT)
	@mess64 megadriv -cart $(OUT) $(MESS_OPTIONS) -debug

-include $(OBJS:.o=.d)
